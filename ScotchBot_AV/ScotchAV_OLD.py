import RPi.GPIO as GPIO
import os
import sys
from subprocess import Popen
#From https://www.hackster.io/ThothLoki/play-video-with-python-and-gpio-a30c7a
GPIO.setmode(GPIO.BCM)

GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP)

movie1 = ("LOOP.mp4")
movie2 = ("ONE.mp4")

last_state1 = True
last_state2 = True

input_state1 = True
input_state2 = True
quit_video = False

player = False
looper = True

while True:
    #Read states of inputs
    input_state1 = GPIO.input(17)
    input_state2 = GPIO.input(18)
    quit_video = GPIO.input(24)
    

    #If GPIO(17) is shorted to Ground
    if input_state1 != last_state1:
        print('ONE.')
        if ((player or looper) and not input_state1):
            os.system('killall omxplayer.bin')
            omxc = Popen(['omxplayer', '-b', movie2])
            player = True
        elif not input_state1:
            omxc = Popen(['omxplayer', '-b', movie2])
            player = True

    #If GPIO(18) is shorted to Ground
    elif input_state2 != last_state2:
        print('TWO.')

        if ((player or looper) and not input_state2):
            os.system('killall omxplayer.bin')
            omxc = Popen(['omxplayer', '-b', movie2])
            player = True
        elif not input_state2:
            omxc = Popen(['omxplayer', '-b', movie2])
            player = True

    #If omxplayer is running and GIOP(17) and GPIO(18) are not shorted to Ground
    elif (input_state1 and input_state2):
        # print('LOOP.')
        if (player):
            print('killall in loop')
            os.system('killall omxplayer.bin')
            player = False
        looper = True
        # omxc = Popen(['omxplayer', '-b', movie1])
        # os.system('omxplayer -o local --no-osd --audio_fifo 0.01 --video_fifo 0.01 --vol 1 --loop LOOP.mp4')
        # omxc = Popen(['omxplayer', '-o', 'local', '--loop', 'LOOP.mp4'])
        # os.system('killall omxplayer.bin')
        # player = True

    #GPIO(24) to close omxplayer manually - used during debug
    if quit_video == False:
        os.system('killall omxplayer.bin')
        player = False

    #Set last_input states
    last_state1 = input_state1
    last_state2 = input_state2

