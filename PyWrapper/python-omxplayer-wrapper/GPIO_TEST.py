from omxplayer import OMXPlayer
from time import sleep
import RPi.GPIO as GPIO
import signal
import subprocess

video1Pin = 17
video2Pin = 18
video3Pin = 22
video4Pin = 27

debounceTime = 500

triggerPins = [video1Pin,video2Pin,video3Pin,video4Pin]
state = 0 # 0 is loop, 1 is 1, 2 is 2
clipStarts= [00.000, 10.000, 12.020, 15.030, 17.060]
clipEnds = [10.000, 12.020, 15.030, 17.060, 27.000]

VID_START = clipStarts[0]
VID_END   = clipEnds[0]

loop_point = VID_END

subprocess.call(["pkill", "-9", "omxplayer"])




file_path_or_url = '/usr/share/adafruit/webide/repositories/my-pi-projects/ScotchBot_AV/CAPTERED.mp4'
args=['--loop', '--no-keys', '-b']
player = OMXPlayer(file_path_or_url, args)

def _on_press(channel):
    print "NEW falling edge detected on %r" % channel
    state = triggerPins.index(channel)+1
    print "state is %r" % state
    VID_END = clipEnds[state]
    VID_START=clipStarts[state]
    player.set_position(VID_START)
    # sleep(0.1)


GPIO.setmode(GPIO.BCM)
GPIO.setup(video1Pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(video2Pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(video3Pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(video4Pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)


def kill_all_players(a, b):
    print "killers gonna kill"
    player.quit()
    subprocess.call(["pkill", "-9", "omxplayer"])
    exit()


#Binds an interupt function to the falling edge of the GPIO pin (Button press)
GPIO.add_event_detect(video1Pin, GPIO.FALLING, bouncetime=debounceTime)  
GPIO.add_event_detect(video2Pin, GPIO.FALLING, bouncetime=debounceTime)  
GPIO.add_event_detect(video3Pin, GPIO.FALLING, bouncetime=debounceTime)  
GPIO.add_event_detect(video4Pin, GPIO.FALLING, bouncetime=debounceTime)  

#quits on quit
signal.signal(signal.SIGTERM, kill_all_players)

def checkButtons():
    for pinn in triggerPins:
        if GPIO.event_detected(pinn):
            print "falling edge detected on %r" % pinn
            state = triggerPins.index(pinn)+1
            print "state is %r" % state
            global loop_point 
            loop_point= clipEnds[state]
            player.set_position(clipStarts[state])

while(True):
    player.play()
    checkButtons()
    if player.position() > loop_point:
        loop_point = clipEnds[0]
        player.set_position(clipStarts[0])
        sleep(0.1)
    sleep(0.1)