from getch import getch, pause
from omxplayer import OMXPlayer
from time import sleep
# import msvcrt
import sys
from select import select


# import RPi.GPIO as GPIO
import signal
import subprocess
# import numpy as np

video1Pin = 17
video2Pin = 18
video3Pin = 22
video4Pin = 27

debounceTime = 500

# triggerPins = [video1Pin,video2Pin,video3Pin,video4Pin]
state = 0 # 0 is loop, 1 is 1, 2 is 2

# def start_the_video():
# global loop_point 
subprocess.call(["pkill", "-9", "omxplayer"])
# file_path_or_url = '/usr/share/adafruit/webide/repositories/my-pi-projects/ScotchBot_AV/CAPTERED.mp4'
# file_path_or_url = '/usr/share/adafruit/webide/repositories/my-pi-projects/PyWrapper/python-omxplayer-wrapper/r2-vfr.mp4'
file_path_or_url = '/home/pi/Videos/SEQUENCE_1-sm2.mp4'
args=['--loop', '--no-keys','--no-osd', '-b']
player = OMXPlayer(file_path_or_url, args)

VideoVersionString = 'r2'
fps = 24.0
clipNames = ['neutral',   'cute',      'dizzy',      'hearts',  'joyful',     'sad',      'smelling'  ]
clipStarts= [0.00,        7+( 3/fps),   10 +(2/fps), 15+(3/fps), 18+(4/fps),  21+(8/fps),  26+(7/fps)]
clipEnds =  [6+(20/fps) , 9+(16/fps),   14+(22/fps), 18.0  ,     21+(4/fps),  26+(2/fps),  29+(0/fps)]#was 29
clipTriggers =[ -1,         -1,         0,            1,          2,           3,          4]

loop_point= clipEnds[0]


def kill_all_players(a, b):
    print "killers gonna kill"
    player.quit()
    # subprocess.call(["pkill", "-9", "omxplayer"])
    exit()
    
def GetChar(Block=False):
  if Block or select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], []):
    return sys.stdin.read(1)
#   raise error('NoChar')

#quits on quit
signal.signal(signal.SIGTERM, kill_all_players)
currentPosition = 0

def goTo(target):
    print "state is %r" % target
    print "video is %r" % clipNames[target]
    # print "video duration is %r" % (clipEnds[target]-clipStarts[target])
    global loop_point 
    loop_point= clipEnds[target]
    player.set_position(clipStarts[target])
    # sleep(1)
    return
        

goTo(0);

while(True):
    player.play()
    # print "eggs"
    if player.position() > loop_point:
        loop_point = clipEnds[0]
        player.set_position(clipStarts[0])
        sleep(0.1)
    # checkButtons()
    key = GetChar()
    if (True):
    
        if key == ord('1'): #ESC
            print "you pressed 1!"
            goTo(1)
        elif key == ord('2'): #Enter
            print "you pressed 2!"
            goTo(2)
        elif key == ord('3'): #Special keys (arrows, f keys, ins, del, etc.)
            print "you pressed 3!"
            goTo(3)
        elif key == ord('4'): #Special keys (arrows, f keys, ins, del, etc.)
            print "you pressed 4!"
            goTo(4)
        elif key == ord('5'): #Special keys (arrows, f keys, ins, del, etc.)
            print "you pressed 5!"
            goTo(5)
        elif key == ord('0'): #Special keys (arrows, f keys, ins, del, etc.)
            print "you pressed 0!"
            goTo(0)


    sleep(0.1)

# while(True):

#     checkKeyboard()
#     player.play()
#     currentPos = player.position()
#     print "current position: %f" %  currentPos
#     print "loop_point position: %f" %  loop_point
#     #print player.maximum_rate()
#     if currentPos > loop_point:
#         print "LOOPINGcurrent position: %f" %  currentPos
#         goTo(0)
#         print "video is back to %r" % clipNames[0]
#         # loop_point = clipEnds[0]
#         # player.set_position(clipStarts[0])
#         sleep(1)
#     sleep(0.1)